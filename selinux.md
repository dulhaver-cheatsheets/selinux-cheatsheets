
https://youtu.be/tXNr3gOgrn8

### modes

There are 3 modes

1. `disabled`
1. `enforcing`
1. `permissive`

- `permissive` only shows warnings and can be used for troubleshooting
- `enforcing` does actually protect you

- see them with `sudo getenforce`
- alter the general system setting in `/etc/sysconfig/selinux`
- on the fly use `setenforce permissive/enforcing`

### policies

- `targeted` - targeted processes are protected. Probably good for most scenarios
- `minimum`
- `mls` - multi-level protection

### misc

- `ls -Z` - shows selinux context

```
[gwagner@vm-414001-0365 ~]$ ls -lZ /
total 24
drwxr-xr-x.  11 root root system_u:object_r:default_t :s0     140 Mar 17 10:09 adm
dr-xr-xr-x.   2 root root system_u:object_r:mnt_t:s0           6 Aug 10  2021 afs
lrwxrwxrwx.   1 root root system_u:object_r:bin_t:s0           7 Aug 10  2021 bin -> usr/bin
dr-xr-xr-x.   5 root root system_u:object_r:boot_t:s0       4096 Mar 17 10:08 boot
drwxr-xr-x.  19 root root system_u:object_r:device_t:s0     3460 Mar 21 11:10 dev
drwxr-xr-x. 109 root root system_u:object_r:etc_t:s0        8192 Apr 16 10:11 etc
drwxr-xr-x.  22 root root system_u:object_r:home_root_t:s0  4096 Mar 22 14:02 home
...
```

where: `system_u` -> **user**   |   `object_r` -> **role**   |   `default_t`  ->  **type**

mainly you work with *type* at first

processes also have selinux types.
see them with `ps -Zaux`:
```
[gwagner@vm-414001-0365 ~]$ ps -Zaux | head
LABEL                           USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
system_u:system_r:init_t:s0     root           1  0.0  0.8 172580 16576 ?        Ss   Mar21   2:00 /usr/lib/systemd/systemd rhgb verbose --switched-root --system --deserialize 31
system_u:system_r:kernel_t:s0   root           2  0.0  0.0      0     0 ?        S    Mar21   0:00 [kthreadd]
system_u:system_r:kernel_t:s0   root           3  0.0  0.0      0     0 ?        I<   Mar21   0:00 [rcu_gp]
system_u:system_r:kernel_t:s0   root           4  0.0  0.0      0     0 ?        I<   Mar21   0:00 [rcu_par_gp]
system_u:system_r:kernel_t:s0   root           5  0.0  0.0      0     0 ?        I<   Mar21   0:00 [slub_flushwq]
system_u:system_r:kernel_t:s0   root           6  0.0  0.0      0     0 ?        I<   Mar21   0:00 [netns]
system_u:system_r:kernel_t:s0   root           8  0.0  0.0      0     0 ?        I<   Mar21   0:00 [kworker/0:0H-events_highpri]
system_u:system_r:kernel_t:s0   root          10  0.0  0.0      0     0 ?        I<   Mar21   0:00 [mm_percpu_wq]
system_u:system_r:kernel_t:s0   root          12  0.0  0.0      0     0 ?        I    Mar21   0:00 [rcu_tasks_kthre]
```